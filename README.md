# This is api project that deals with todos

# Installation steps
## install apex client
## configure default AWS profile
## install packages ```npm run build``` (*)
## to deploy all functions, run: ```apex deploy```

[apex interaction examples](docs/apex-examples.md)

[RESTful interaction examples](docs/RESTfull-interactions.md)

[swagger file for restful api](docs/swagger.json)

(*) if cannot download todoservice library: ```mv ~/.nmprc ~/.npmrc.bak```

[issue](https://github.com/npm/npm/issues/8380)
