# Configure endpoint, including stage

```
export ENDPOINT=https://kgy6wnk144.execute-api.us-east-1.amazonaws.com/prod
```

# Add a new todo

```
curl -X PUT \
 -d '{"user": "testy.mctester@example.com", "description": "Do something awesome", "priority": 0}' \
 -H"Content-Type: application/json" \
 $ENDPOINT/todos
```
 
# List all todos in the system (first 10, by uuid)

```
curl $ENDPOINT/todos
```

# Update an existing todo

```
curl -X POST \
 -d '{"user": "testy.mctester@example.com", "description": "Do something awesome", "priority": 0, "uuid":"4224d913-f159-4bdc-be85-b3497e4aadfe", "completed": "2016-10-15T14:22:03-04:00"}' \
 -H"Content-Type: application/json" \
 $ENDPOINT/todos/4224d913-f159-4bdc-be85-b3497e4aadfe
```

# Delete an existing todo

```
curl -X DELETE \
 -H"Content-Type: application/json" \
 $ENDPOINT/todos/4224d913-f159-4bdc-be85-b3497e4aadfe
```
