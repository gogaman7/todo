# Below are apex invocation examples

# add/update todo
```
echo -n '{"user": "testy.mctester@example.com", "description": "Do something awesome", "priority": 0}' | apex invoke add
```

# list all todos in the system (first 10, by uuid)
```
apex invoke list
```

# delete todo
```
echo -n '{"uuid": "aaec6420-6272-46cf-8be3-b6610cf0989c"}' | apex invoke delete
```

# send out daily emails to users with 1+ incomplete todos
```
apex invoke daily
```
