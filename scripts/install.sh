#!/bin/bash

set -e

# This script install dependencies for all functions

export BASE=$(pwd)

## our functions
declare -a functions=("add" "delete" "list", "daily")

for i in "${functions[@]}"
do
  echo installing packages for function: "$i" ...

  cd $BASE
  cd functions/$i
  npm install

done
