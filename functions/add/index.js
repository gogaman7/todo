// 3rd party
var uuid = require('uuid')

// our libs
var todo = require('todoservice').todoService

/**
 * Function to add todos
 * @param e object like so: { user: 'ibresume@gmail.com', description: 'rule', priority: 1 }
 * @param ctx
 * @param cb
 */
exports.handle = function (e, ctx, cb) {
    console.log(ctx.functionName + ': [e,ctx,cb]: ' + JSON.stringify([e, ctx, cb]))

    if (!e.uuid) {
        // add uuid, if missing
        e.uuid = uuid.v4()
    }
    todo.add(e, function (err) {
        if (err) {
            cb(err, {response: 'failed to add/update'})
        } else {
            cb(err, this.e)
        }
    }.bind({e: e}))
}
