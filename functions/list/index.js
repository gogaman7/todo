// our libs
var todo = require('todoservice').todoService

/**
 * Function to list all todos
 *  * note, only first 10 will be returned
 * @param e
 * @param ctx
 * @param cb
 */
exports.handle = function (e, ctx, cb) {
    console.log(ctx.functionName + ': [e,ctx,cb]: ' + JSON.stringify([e, ctx, cb]))

    todo.list(function (err, arrayOfTodos) {
        cb(err, {todos: arrayOfTodos})
    })
}
