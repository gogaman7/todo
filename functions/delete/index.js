// our libs
var todo = require('todoservice').todoService

/**
 * Function to delete todos
 * @param e object like so: { uuid: '8fea7cb2-6f6d-412e-aaa9-010244e564ed'}
 * @param ctx
 * @param cb
 */
exports.handle = function (e, ctx, cb) {
    console.log(ctx.functionName + ': [e,ctx,cb]: ' + JSON.stringify([e, ctx, cb]))

    todo.delete(e, function (err) {
        if (err) {
            cb(err, {response: 'failed to delete a todo'})
        } else {
            cb(err, {response: 'todo was successfully deleted'})
        }
    })
}
