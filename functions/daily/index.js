// our libs
var incompleteToDoService = require('todoservice').incompleteToDoService

/**
 * Function to send out emails to users with 1+ incomplete todo
 * @param e object like so: { uuid: '8fea7cb2-6f6d-412e-aaa9-010244e564ed'}
 * @param ctx
 * @param cb
 */
exports.handle = function (e, ctx, cb) {
    console.log(ctx.functionName + ': [e,ctx,cb]: ' + JSON.stringify([e, ctx, cb]))

    incompleteToDoService.notifyAllUsers(function (err) {
        if (err) {
            cb(err, {response: 'failed to notify all users'})
        } else {
            cb(err, {response: 'all users have been successfully notified'})
        }
    })
}
